# -*- coding: utf-8 -*-

from __future__ import unicode_literals
import os
import time
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait as wait

filename = os.path.join(os.path.dirname(__file__), 'last_link.txt')


def get_last_link():
	with open(filename) as file: 
		last_link = file.read()
		file.close()
		return last_link

def save_last_link(link):
	with open(filename, "w") as file: 
		file.write(link)
		file.close()

def scrap(driver, page_url, prefill_form, last_link):

	driver.get(page_url)
	elem_links = driver.find_elements_by_xpath("//h2[contains(@class, 'fs14')]/a")

	links = []
	for elem_link in elem_links:
		links.append(elem_link.get_attribute('href'))

	print("{} Last link: {}".format(datetime.now(), last_link))
	save_last_link(links[0])

	for link in links:
		if link != last_link:
			print("{} Scrap url: {}".format(datetime.now(), link))
			
			try:
				driver.get(link)

				# Clic on button adreply_footer_btn
				elem_btn = driver.find_element_by_id("adreply_footer_btn")
				ActionChains(driver).move_to_element(elem_btn).click(elem_btn).perform()
				time.sleep(1)

				# Clic and fill input name
				elem_input_name = driver.find_element_by_xpath("//input[contains(@class, 'input-block-level') and contains(@name, 'name')]")
				elem_input_name.send_keys(prefill_form['name'])
				time.sleep(1)

				# Clic and fill input email
				elem_input_email = driver.find_element_by_xpath("//input[contains(@class, 'input-block-level') and contains(@name, 'email')]")
				elem_input_email.send_keys(prefill_form['email'])
				time.sleep(1)

				# Clic and fill input phone
				elem_input_email = driver.find_element_by_xpath("//input[contains(@type, 'text') and contains(@name, 'phone')]")
				elem_input_email.send_keys(prefill_form['phone'])
				time.sleep(1)

				# Clic and fill textarea adreply_body
				elem_input_adreply_body = driver.find_element_by_xpath("//textarea[contains(@class, 'input-block-level') and contains(@id, 'adreply_body')]")
				elem_input_adreply_body.send_keys(prefill_form['message_begin'])
				elem_input_adreply_body.send_keys(Keys.RETURN)
				time.sleep(1)
				elem_input_adreply_body.send_keys(prefill_form['message_content'])
				elem_input_adreply_body.send_keys(Keys.RETURN)
				elem_input_adreply_body.send_keys(Keys.RETURN)
				time.sleep(1)
				elem_input_adreply_body.send_keys(prefill_form['message_end'])
				elem_input_adreply_body.send_keys(Keys.RETURN)
				time.sleep(1)
				elem_input_adreply_body.send_keys(prefill_form['message_end_bis'])
				time.sleep(1)

				# Clic on checkbox cc
				elem_input_checkbox = driver.find_element_by_xpath("//input[contains(@type, 'checkbox') and contains(@name, 'cc')]")
				ActionChains(driver).move_to_element(elem_input_checkbox).click(elem_input_checkbox).perform()
				time.sleep(1)

				# Mouseover and Clic on btn adreply_footer_btn
				elem_btn_submit = driver.find_element_by_xpath("//input[contains(@type, 'submit') and contains(@id, 'ar_form_send')]")
				time.sleep(1)
				ActionChains(driver).move_to_element(elem_btn_submit).click(elem_btn_submit).perform()
				time.sleep(1)

				# Compare div popup after submit message
				div_contactThanks = driver.find_element_by_id("contactThanks")
				div_contactThanks_class = div_contactThanks.get_attribute("class")
				if (div_contactThanks_class != "modal hide fade in"):
					raise Exception("Error on submit message")

				time.sleep(5)
				print("Message sent")

			except Exception as e:
				print(e)
				print("{} Error on url: {}".format(datetime.now(), link))
		else:
			break

	driver.quit()



if __name__ == '__main__':
    
	chromedriver = "./chromedriver"
	os.environ["webdriver.chrome.driver"] = chromedriver
	driver = webdriver.Chrome(chromedriver)

	page_url = "https://www.avito.ma/fr/maroc/voitures-%C3%A0_vendre"
	prefill_form = {
		"name": "Badre Harkaoui",
		"email": "baddre.harkaoui@gmail.com",
		"phone": "0638119616",
		"message_begin": "Bonjour,",
		"message_content": "Je souhaite louer votre voiture pour un certain temps. Vous pourrez gagner de l’argent le temps de la vendre. Afin d'avoir une assurance tous risques, pouvez-vous inscrire votre véhicule sur benatna (.net) ?",
		"message_end": "Merci !",
		"message_end_bis": "Badre"
	}
	last_link = get_last_link()

	scrap(driver, page_url, prefill_form, last_link)